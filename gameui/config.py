from meta import Singleton

class Config(object):
    __metaclass__ = Singleton
    def __init__(self, *args, **kwargs):
        super(Config, self).__init__(*args, **kwargs)
        self.fg = "white"
        self.bg = "black"
        self.fg_s = "black"
        self.bg_s = "white"
