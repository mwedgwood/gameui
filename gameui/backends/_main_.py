from subprocess import Popen, PIPE

# Interact with the system environment
class Environment(object):
    __metaclass__ = Singleton
    def set_screen_resolution(self, prefw, prefh):
        """Set the screen resolution to the smallest available
        resolution above the given resolution"""
        def area(x,y): return x * y 
        (current, resolutions) = self.screen_resolutions
        (newW, newH) = current
        for (width,height) in resolutions:
            if width >= prefw and height >= prefh and area(width,height) <= area(newW,newH):
                newW = width
                newH = height
        if (newW,newH) != (width,height):
            xrandr = Popen(('xrandr','-s','{0}x{1}'.format(newW,newH)))
    @property
    def screen_resolutions(self):
        """A list of screen resolutions supported by this system"""
        # Use xrandr to list the supported resolutions
        # TODO: capture/mute xrandr's stderr output
        xrandr = Popen(('xrandr',), stdout=PIPE)
        grep = Popen(('grep','^ ',), stdin=xrandr.stdout, stdout=PIPE)
        xrandr.stdout.close()
        output = grep.communicate()[0]
        # build list of resolutions
        resolutions = []
        current = None
        for line in output.split("\n"):
            try:
                resolution = line.split()[0]
                (width, height) = resolution.split('x')
                width = int(width)
                height = int(height)
                resolutions.append((width, height))
                if "*" in line: current = (width, height)
            except:
                pass
        return (current, resolutions)
