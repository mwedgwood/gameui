from meta import Singleton
import os
import sqlite3

# Abstract class for game systems
class System(object):
    __metaclass__ = Singleton
    _cachedir = os.path.expanduser("~/.gameui/cache")
    def __init__(self, *args, **kwargs):
        super(System, self).__init__(*args, **kwargs)
        if not os.path.isdir(self._cachedir):
            os.makedirs(self._cachedir)
    def play(self, rom):
        """Run the given rom"""
        raise NotImplementedError()
    def load_one(self, rom):
        """Generate a Game object for the given ROM"""
        raise NotImplementedError()
    def load_all(self):
        """Produce Games for all runnable ROMs known to this system"""
        raise NotImplementedError()
    @property
    def _cacheDbFile(self):
        return os.path.join(self._cachedir,"{0}.db".format(self.__class__.__name__))
    def _getDbConnection(self):
        return sqlite3.connect(self._cacheDbFile)
    def rescan(self):
        """Rebuild the list of available games."""
        raise NotImplementedError()

class GamePlayFailedException(Exception): pass

class Game(object):
    def __init__(self, system, name, details={}):
        super(Game, self).__init__()
        self._system = system
        self._name = name
        # ideally, details will contain:
        #   description, manufacturer, year
        #   res_w, res_h, players
        self._details = details
    def play(self):
        """Arrange for the is rom to be run"""
        return self._system.play(self._name)
    def __str__(self):
        return "<Game ({0}/{1})>".format(self._system.sys_id, self._name)
    @property
    def name(self):
        return self._name
    @property
    def title(self):
        raise NotImplementedError()
    @property
    def res_w(self):
        raise NotImplementedError()
    @property
    def res_y(self):
        raise NotImplementedError()
