import Tkinter as tk
from gameui.config import Config

class RowItem(tk.Label):
    def __init__(self, parent, data, **options):
        options['text'] = data
        tk.Label.__init__(self, parent, **options)
        self.data = data
    def activate(self):
        print "Activate (%s)" % self.cget('text')
    def deactivate(self):
        print "Deactivate (%s)" % self.cget('text')

class TenFoot(tk.Frame):
    def __init__(self, parent, datasource, num_rows, row_class=RowItem, **options):
        tk.Frame.__init__(self, parent, **options)
        self._ds = datasource
        self._num_rows = num_rows
        self._row_class = row_class
        self._rows = []
        self._reload()
        self.columnconfigure(0, weight=1)
    def _reload(self, dsi=0, si=0):
        """remove all rows and re-populate"""
        self._dsi = dsi 
        self._si = si
        for num in range(len(self._rows)):
            self._rows.pop().grid_forget()
        for index in range(self._num_rows):
            try:
                new_row = self._row_class(self, self._ds[self._dsi + index])
            except IndexError:
                break
            new_row.grid(sticky="we",row=index)
            self._rows.append(new_row)
        self._update_selection()
    def _scroll_up(self):
        # scroll the list up one row, if possible
        if self._dsi == 0:
            return
        else:
            self._dsi -= 1
            self._si += 1
            new_row = self._row_class(self, text=self._ds[self._dsi])
            self._rows.pop().grid_forget()
            for row in self._rows:
                row.grid_configure(row=int(row.grid_info()['row']) + 1)
            new_row.grid(sticky="we",row=0)
            self._rows.insert(0, new_row)
            self._update_selection()
    def _scroll_down(self):
        # scroll the list down one, if possible
        max_dsi = len(self._ds) - self._num_rows
        if self._dsi == max_dsi:
            return
        else:
            #import pdb; pdb.set_trace()
            new_row = self._row_class(self, self._ds[self._dsi + self._num_rows])
            self._dsi += 1
            self._si -= 1
            self._rows.pop(0).grid_forget()
            for row in self._rows:
                row.grid_configure(row=int(row.grid_info()['row']) - 1)
            new_row.grid(sticky="we",row=self._num_rows - 1)
            self._rows.append(new_row)
            self._update_selection()
    def _update_selection(self):
        config = Config()
        for index in range(len(self._rows)):
            # TODO: update row styles
            if index == self._si:
                self._rows[index].configure(fg=config.fg_s,bg=config.bg_s)
            else:
                self._rows[index].configure(fg=config.fg,bg=config.bg)
    @property
    def height(self):
        return self._num_rows
    def nav_next(self, event=None):
        new_si = self._si + 1
        if new_si == len(self._rows):
            # gone beyond the last row, loop to the top
            self._reload()
        else: 
            self._si = new_si
            self._update_selection()
            if new_si == self._num_rows - 1:
                self._scroll_down()
        print "si=%s, dsi=%s" % (self._si, self._dsi)
    def nav_prev(self, event=None):
        new_si = self._si - 1
        if new_si == -1:
            # gone beyond the first row, loop to the bottom
            new_dsi = len(self._ds) - self._num_rows
            new_si = self._num_rows - 1
            self._reload(dsi=new_dsi,si=new_si)
            #self._si = self._num_rows - 1
            #self._update_selection()
        else: 
            self._si = new_si
            self._update_selection()
            if new_si == 0:
                self._scroll_up()
        print "si=%s, dsi=%s" % (self._si, self._dsi)
    def select(self, event=None):
        self._rows[self._si].activate()
    def back(self, event=None):
        self._rows[self._si].deactivate()
