import argparse
import Tkinter as tk
import tkMessageBox
from widgets import TenFoot, RowItem
from gameui.config import Config

class App(object):
    def __init__(self):
        config = Config()
        self.invocation()
        self.root = tk.Tk()
        if config.fullscreen:
            w, h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
            #self.root.overrideredirect(1)
        else:
            w, h = 600, 400
        self.root.geometry("%dx%d+0+0" % (w, h))
        self.menu_init()
        self.layout()
        self.bind_keys()
        self.root.lift()
        self.root.focus_set()
    def invocation(self):
        parser = argparse.ArgumentParser(
                description="A lightweight joystick-driven user interface for game cabinets")
        parser.add_argument('-w','--windowed',action='store_false',dest='fullscreen')
        parser.parse_args(namespace=Config())
    def menu_init(self):
        self.menus = [
                ("MAME","NES","SNES","N64","MAME","NES","SNES","N64","MAME","NES","SNES","N64",),
            ]
    def layout(self):
        self.root.configure(background=Config().bg)
        height = 12 # TODO: calculate and parameterize
        self.tenfoot = TenFoot(self.root, self.menus[0], height)
        self.root.columnconfigure(0, weight=1, pad=10)
        self.tenfoot.grid(sticky="nsew")
    def bind_keys(self):
        # Bindings based on i-PAC 4 defaults
        bindings = {
            # Up
            'Key-Up': self.tenfoot.nav_prev,
            'r': self.tenfoot.nav_prev,
            'i': self.tenfoot.nav_prev,
            'y': self.tenfoot.nav_prev,
            # Down
            'Key-Down': self.tenfoot.nav_next,
            'f': self.tenfoot.nav_next,
            'k': self.tenfoot.nav_next,
            'n': self.tenfoot.nav_next,
            # Forward (right)
            'Key-Right': self.tenfoot.select,
            'g': self.tenfoot.select,
            'l': self.tenfoot.select,
            'u': self.tenfoot.select,
            # Back (left)
            'Key-Left': self.tenfoot.back,
            'd': self.tenfoot.back,
            'j': self.tenfoot.back,
            'v': self.tenfoot.back,
            # Others
            'Escape': self.confirm_exit,
        }
        for key, action in bindings.items():
            btn = "<%s>" % key
            self.root.bind(btn, action)
            self.tenfoot.bind(btn, action)
    def confirm_exit(self, event):
        if tkMessageBox.askyesno("Quit?", "Are you sure you'd like to quit?"):
            self.root.destroy()

def main():
    app = App()
    app.root.mainloop()
