from abstract import System
import sqlite3
import re
from game import Game, GamePlayFailedException
from subprocess import Popen, PIPE
from xml.parsers.expat import ParserCreate

class MAMESystem(System):
    sys_id = "MAME"
    def __init__(self,*args,**kwargs):
        super(MAMESystem, self).__init__(*args, **kwargs)
        # TODO: Implement a configuration system
        self._executable = "/home/mew/MAME/bin/mame"
        self._cfgdir = "/home/mew/MAME/cfg"
    def _exec(self,*args):
        # run the MAME executable with the given arguments
        # returns the status and output (stdout)
        cmd = [self._executable, "-inipath", self._cfgdir] + list(args)
        job = Popen(cmd, stdout=PIPE)
        output = job.communicate()[0]
        return (job.returncode, output)
    def play(self, game):
        (status, data) = self._exec(rom)
        if status != 0:
            raise GamePlayFailedException()
    def load_one(self, rom):
        game = None
        db = self._getDbConnection()
        db.row_factory = sqlite3.Row
        details = db.execute("select * from games where name = ?", (rom,))
        if len(details) == 1:
            game = Game(self, rom, details.fetchone())
        db.close()
        return game
    def load_all(self):
        games = []
        db = self._getDbConnection()
        db.row_factory = sqlite3.Row
        details_set = db.execute(
            "select * from games where name in (select name from roms)"
        )
        for details in details_set.fetchall():
            games.append(Game(self, details['name'], details))
        return games
    def rescan(self):
        # TODO: provide signals for each step, progress
        self._refresh_game_cache()
        self._scan_roms()
    def _scan_roms(self):
        # produce a list of playable roms
        # this takes a while
        db = self._getDbConnection()
        try:
            db.execute('drop table roms;')
        except:
            pass
        db.execute("""
            create table roms (
                name varchar(20)
            );""")
        romline = re.compile("romset (?P<rom>\S+) is (?P<status>.*)")
        output = self._exec("-verifyroms")[1]
        for line in output.split("\n"):
            info = romline.match(line)
            if info is None: continue
            if info.group("status") in ("good","best available"):
                db.execute("insert into roms values (?);", (info.group('rom'),))
        db.commit()
        db.close()
    def _refresh_game_cache(self):
        """Dump the interesting parts of mame -listxml into a database"""
        # initialize the database
        db = self._getDbConnection()
        parser = _MAMEXMLParser(db, 'games')
        xmlstr = self._exec('-listxml')[1]
        parser.parse(xmlstr)
        db.close()

class _MAMEXMLParser(object):
    DEF_DISP_W = 640
    DEF_DISP_H = 400
    COMMIT_AT = 100
    def __init__(self, db, table):
        super(_MAMEXMLParser, self).__init__()
        self._db = db
        self._table = table
        self._parser = ParserCreate()
        self._parser.StartElementHandler = self.start_element
        self._parser.EndElementHandler = self.end_element
        self._parser.CharacterDataHandler = self.char_data
        self._ic = 0
        self._reset()
    def _reset(self):
        self._in_game = False
        self._in_desc = False
        self._in_year = False
        self._in_manf = False
        self._attributes = {
            'description': '',
            'year': '0000',
            'manufacturer': 'Unknown',
            'res_w': self.DEF_DISP_W,
            'res_h': self.DEF_DISP_H,
            'disp_type': 'unknown',
            'players': 1,
        }
    def parse(self, xmldata):
        try:
            self._db.execute('drop table {0};'.format(self._table))
        except: pass
        self._db.execute("""
            create table {0} (
                name varchar(20) primary key,
                description varchar(255),
                year char(4),
                manufacturer varchar(255),
                res_w int,
                res_h int,
                disp_type varchar(10),
                players int
            );""".format(self._table))
        self._parser.Parse(xmldata)
        self._db.commit()
    def start_element(self, name, attrs):
        if name == 'game':
            if attrs.get('isbios','no') == 'no' and attrs.get('isdevice','no') == 'no' and attrs.get('cloneof') is None:
                self._in_game = True
                self._attributes['name'] = attrs.get('name')
        elif not self._in_game:
            return
        elif name == 'description':
            self._in_desc = True
        elif name == 'year':
            self._in_year = True
        elif name == 'manufacturer':
            self._in_manf = True
        elif name == 'display':
            self._attributes['res_w'] = attrs.get('width', self.DEF_DISP_W)
            self._attributes['res_h'] = attrs.get('height', self.DEF_DISP_H)
            self._attributes['disp_type'] = attrs.get('type')
        elif name == 'input':
            self._attributes['players'] = attrs.get('players')
    def end_element(self, name):
        if name == 'description':
            self._in_desc = False
        elif name == 'year':
            self._in_year = False
        elif name == 'manufacturer':
            self._in_manf = False
        elif name == 'game':
            if self._in_game:
                # persist the entry
                self._db.execute(
                    """insert into games values (:name, :description,
                    :year, :manufacturer, :res_w, :res_h, :disp_type,
                    :players);""", self._attributes)
                self._ic += 1
                if self._ic > self.COMMIT_AT:
                    self._db.commit()
                    self._ic = 0
            self._in_game = False
    def char_data(self, data):
        if self._in_desc:
            self._attributes['description'] = data
        elif self._in_year:
            self._attributes['year'] = data
        elif self._in_manf:
            self._attributes['manufacturer'] = data
