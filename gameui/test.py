#!/usr/bin/env python

from backends.mame import MAMESystem

def test_mame():
    m = MAMESystem()
    m.rescan()
    G = m.load_all()
    for g in G:
        print g.name
